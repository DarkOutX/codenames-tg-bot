const { restart } = require("nodemon");
const {
    Sessions,
    Teams
} = require("../db/models")

const Game = require('./Game');

class User {
    
/* 
    static isCaptain = async (userId, chatId) => {
        return await Game.getMainData(chatId, ['CAPTAIN_RED', 'CAPTAIN_BLUE'])
                .then(captains => {
                    console.log(captains)
                    if(captains)
                        return (userId == captains.CAPTAIN_RED || userId == captains.CAPTAIN_BLUE)
                    else
                        return false
                })
    }
 */
    static getTeam = (userId, chatId) => {
        return Teams.findOne({
            where: {
                CHAT_ID: chatId,
                USER_ID: userId,
            },
            raw: true
        }).then(user => {
            return (user)?user.COLOR:null
        })
    }

    static setTeam = (userData, chatId, teamColor) => {
        Teams.findOne({
            where: {
                CHAT_ID: chatId,
                USER_ID: userData.id,
            },
            raw: true
        }).then(user => {

            const newRecord = {
                CHAT_ID: chatId,
                USER_ID: userData.id,
                USERNAME: userData.username,
                NAME: `${userData.first_name} ${userData.second_name || ""}`.trim(),
                COLOR: (teamColor == "🔴") ? "🔴" : "🔵"
            }

            if (user) {
                return Teams.update(
                    newRecord, 
                    { where: {
                        CHAT_ID: chatId,
                        USER_ID: userData.id,
                    }}
                )
            } else {
                return Teams.create(newRecord)
            }
        })
    }

}

module.exports = User;