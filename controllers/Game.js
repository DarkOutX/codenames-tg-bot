const {
    Sessions,
    Teams
} = require("./../db/models")

const Images = require('./Images')

const SYMBOLS = {
    "🔴": "r",
    "🔵": "b",

    "🟥": "r", // red
    "🟦": "b", // blue
    "🟨": "n", // neutral
    "⬛️": "k", // killer

    "r": "🟥",
    "b": "🟦",
    "n": "🟨",
    "k": "⬛️",
}

const ANSWER_RESULTS = {
    SUCCESS: 777,
    WRONG_TEAM: 5,
    NEUTRAL: 1,
    KILLER: 666
}

const generateRandomNums = (amount, max) => {

    let nums = [];
    for (let i = 0; i <= max; i++) nums.push(i);

    let shuffled = nums.sort(() => 0.5 - Math.random());

    return shuffled.slice(0, amount);

}

const generateKey = (firstTeam) => {

    let key = [
        "b", "b", "b", "b", "b",
        "b", "b", "r", "r", "r",
        "r", "r", "r", "r", "k",
        "n", "n", "n", "n"
    ];

    if (firstTeam != "🔴" && firstTeam != "🔵") firstTeam = "🔵";
    key.push(SYMBOLS[firstTeam]);
    key.sort(() => 0.5 - Math.random());

    return key.join("")

}

class Game {

    static end = (winner, type) => {

    }

    static destroy = (chatId) => {
        return Sessions.findOne({
            where: {
                ID: chatId
            }
        }).then((res) => {
            if (res) Sessions.destroy({
                where: {
                    ID: chatId
                }
            })
        })
    }

    static create = async (chatId) => {

        await this.destroy(chatId);

        const
            firstTeam = (Math.round(Math.random())) ? "🔴" : "🔵",
            key = generateKey(firstTeam),
            field = generateRandomNums(20, 278).join();

        await Images.generate(key, chatId, true)
        await Images.generate(field, chatId, false)

        return Sessions.create({
            ID: chatId,
            KEY: key,
            FIELD: field,
            TEAM_TURN: firstTeam
        }, {
            raw: true
        })

    }

    static getMainData = (chatId, attributes = ['KEY', 'FIELD', 'ATTEMPTS', 'TEAM_TURN', 'CAPTAIN_RED', 'CAPTAIN_BLUE']) => {

        return Sessions.findOne({
            where: {
                ID: chatId,
            },
            attributes: attributes,
            raw: true
        }).then(res => {
            if (res && res.KEY) res.KEY = res.KEY.split("")
            if (res && res.FIELD) res.FIELD = res.FIELD.split(",")
            return res
        })

    }

    static getTeams = async (chatId) => {
        let captains = await this.getMainData(chatId, ['CAPTAIN_RED', 'CAPTAIN_BLUE'])
        return await Teams
            .findAll({
                where: {
                    CHAT_ID: chatId,
                },
                raw: true
            })
            .then((teams) => {
                teams.forEach((user) => {
                    user.isCaptain = user.USER_ID == captains.CAPTAIN_RED || user.USER_ID == captains.CAPTAIN_BLUE
                })
                return teams
            })
    }

    static handleAnswer = (type, curTeam, hiddenAgents) => {

        const
            oppositeTeam = (curTeam == "🔴") ? "🔵" : "🔴";

        switch (type) {
            case ANSWER_RESULTS.KILLER:
                // Opposite Team win the game
                return {
                    end: true,
                        winner: oppositeTeam
                }
                case ANSWER_RESULTS.SUCCESS:
                    // If no more hidden agents, current team win the game
                    if (hiddenAgents[SYMBOLS[curTeam]] == 0) {
                        return {
                            end: true,
                            winner: curTeam
                        }
                    } else {
                        return {
                            end: false,
                            endTurn: false,
                        }
                    }
                    case ANSWER_RESULTS.WRONG_TEAM:
                        // If no more hidden agents, opposite team win the game
                        if (hiddenAgents[SYMBOLS[oppositeTeam]] == 0) {
                            return {
                                end: true,
                                winner: oppositeTeam
                            }
                        } else {
                            return {
                                end: false,
                                endTurn: true,
                            }
                        }
                        case ANSWER_RESULTS.NEUTRAL:
                            // Just ending turn 
                            return {
                                end: false,
                                    endTurn: true,
                            }
                            default:
                                new Error("Can't handle unknown answer")
        }

    }

    static checkAnswer = (type, team) => {

        team = (team == "🔴") ? "r" : "b";

        switch (type) {
            case team:
                return ANSWER_RESULTS.SUCCESS
            case SYMBOLS["🟥"]:
            case SYMBOLS["🟦"]:
                // wrong answer coz we can't reach this line if answered correctly
                return ANSWER_RESULTS.WRONG_TEAM
            case SYMBOLS["🟨"]:
                return ANSWER_RESULTS.NEUTRAL
            case SYMBOLS["⬛️"]:
                return ANSWER_RESULTS.KILLER
            default:
                new Error("Answer got an incorrect value")
        }

    }

    static setCaptain = (userId, chatId, color) => {
        return Sessions.findOne({
            where: {
                ID: chatId,
            },
            raw: true
        }).then(session => {
            if (session) {
                const param = (color == "🔴") ? "CAPTAIN_RED" : "CAPTAIN_BLUE";
                return Sessions.update({
                    [param]: userId
                }, {
                    where: {
                        ID: chatId,
                    }
                })
            } else {
                return false
            }
        })
    }

    static setAttempts = (chatId, amount) => {
        return Sessions.update({
                ATTEMPTS: amount,
            }, {
                where: {
                    ID: chatId
                }
            })
            .catch(e => console.log(e))
    }

    static update = async (chatId, pos) => {

        const
            gameData = await this.getMainData(chatId),
            oppositeTeam = (gameData.teamTurn == "🔴") ? "🔵" : "🔴",
            attempts = (gameData.ATTEMPTS > 1) ? --gameData.ATTEMPTS : 0,
            hiddenAgents = {
                r: 0,
                b: 0,
            },
            key = gameData.KEY,
            answerResult = this.checkAnswer(key[pos], gameData.teamTurn); // Sending current team

        let field = gameData.FIELD;
        // Revealing value from key to visible field
        field[pos] = key[pos]

        key.map((elem, i) => {
            // Counts all unopened r(Red agent) and b(Blue agent) in key
            if ((elem == SYMBOLS["🟦"] || elem == SYMBOLS["🟥"]) && elem != field[i]) {
                hiddenAgents[elem]++
            }
        })

        const turnResult = this.handleAnswer(answerResult, gameData.teamTurn, hiddenAgents);

        if (turnResult.end) {
            // Game ends
            this.destroy(chatId)
            return {
                winner: turnResult.winner,
                type: answerResult
            }
        } else {
            // Game continues
            return Sessions.update({
                FIELD: field.join(),
                ATTEMPTS: (turnResult.endTurn) ? 0 : attempts,
                TEAM_TURN: (turnResult.endTurn || attempts < 1) ? oppositeTeam : gameData.teamTurn,
            }, {
                where: {
                    ID: chatId
                }
            }).then(res => console.log(res))
        }

    }

    static begin = (chatId) => {
        return this.create(chatId)
    }

}

module.exports = Game;