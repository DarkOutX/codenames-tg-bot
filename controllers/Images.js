const createCollage = require("@settlin/collage")
const fs = require('fs')

const
    IMGS_DIR = './imgs/',
    SAVE_DIR = "./games/";

class Images {

    static generate = async (list, chatId, isKey = false) => {

        let
            srcSave = `${SAVE_DIR}${chatId}/`,
            srcList = [];

        list = (isKey) ? list.split("") : list.split(",");

        list.forEach(pic => {
            if (pic == "b" || pic == "r" || pic == "k" || pic == "n") {
                srcList.push(`${IMGS_DIR}roles/${pic}.jpg`);
            } else {
                srcList.push(`${IMGS_DIR}${pic}.png`);
            }
        })

        try {
            if (!fs.existsSync(SAVE_DIR)) fs.mkdirSync(SAVE_DIR);
            if (!fs.existsSync(srcSave)) fs.mkdirSync(srcSave);
        } catch (e) {
            new Error("Can't make path to save")
        }

        srcSave += (isKey) ? "key.png" : "game.png";

        return await new Promise((resolve, reject) => {
            createCollage({
                sources: srcList,
                width: 5,
                height: 4,
                imageWidth: 536,
                imageHeight: 338,
                spacing: 0,
            }).then((canvas) => {
                const stream = fs.createWriteStream(srcSave);
                canvas.jpegStream().pipe(stream)
                stream.on('finish', () => {
                    // Img saved
                    resolve(true);
                });
            })
        })

    }

    static delete = (chatId) => {
        //delete files
    }

}

module.exports = Images;