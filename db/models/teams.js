module.exports = (sequelize, type) => {
    return sequelize.define('teams', {
        USER_ID: {
            type: type.INTEGER,
            primaryKey: true,
        },
        CHAT_ID: {
            type: type.INTEGER,
            primaryKey: true,
        },
        NAME: {
            type: type.STRING
        },
        USERNAME: {
            type: type.STRING
        },
        COLOR: {
            // 🔵 / 🔴 
            type: type.CHAR(2),
            defaultValue: "",
        },
    }, {
        timestamps: false,
    })
}
