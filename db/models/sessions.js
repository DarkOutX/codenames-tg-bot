module.exports = (sequelize, type) => {
    return sequelize.define('games', {
        ID: {
            type: type.INTEGER,
            primaryKey: true,
        },
        KEY: {
            type: type.STRING,
            defaultValue: "",
        },
        FIELD: {
            type: type.STRING,
            defaultValue: "",
        },
        TEAM_TURN: {
            // 🔵 / 🔴 
            type: type.CHAR(2),
            defaultValue: "",
        },
        ATTEMPTS: {
            type: type.INTEGER(1),
            defaultValue: 0
        },
        CAPTAIN_RED: {
            type: type.INTEGER,
            defaultValue: 0
        },
        CAPTAIN_BLUE: {
            type: type.INTEGER,
            defaultValue: 0
        },
    }, {
        timestamps: false,
    })
}
