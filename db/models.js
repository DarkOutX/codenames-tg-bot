const { Sequelize } = require('sequelize')
const dbConnect = require('./connect');

// Getting model templates
const 
    sessions_model = require('./models/sessions'),
    teams_model = require('./models/teams');

// Connecting models to ORM
const
    Sessions = sessions_model(dbConnect, Sequelize),
    Teams = teams_model(dbConnect, Sequelize);

// Creating tables if not exist
dbConnect.sync()

module.exports = {
    Sessions: Sessions,
    Teams: Teams,
}
