require('dotenv').config();
const Telegraf = require('telegraf')
const Session = require('telegraf/session')
const getUserData = require('./middlewares/getUserData')

const fs = require('fs')

const db = require('./db/connect')

const Game = require('./controllers/Game')
const User = require('./controllers/User')

Game
    .create(718044989)
    .then((res) => {
        Game
            .update(718044989, 3)
    })

/*
    TODO:

    No 2 caps of same team
    Check which teams turn, show remaining attempts
        Prevent user of another team to select card
        On mistake, turn ends
        After first attempt, show "End turn" button
    
*/

const bot = new Telegraf(process.env.TG_API_KEY);

const picSelectMenu = (field) => {
    let keyboard = [];

    for (let i = 0; i < 20; i++) {
        let obj = {
            text: '🏞',
            callback_data: 'pic_select_' + i
        }
        if (field[i] == "b") obj.text = '🟦';
        if (field[i] == "r") obj.text = '🟥';
        if (field[i] == "n") obj.text = '🟨';
        if (field[i] == "k") obj.text = '⬛️';
        if (field[i] == "b" || field[i] == "r" || field[i] == "n" || field[i] == "k") obj.callback_data = "empty_select";

        keyboard.push(obj);
    }

    return Telegraf.Extra
        .markdown()
        .markup((m) => m.inlineKeyboard(keyboard, {
            columns: 5
        }));
}

const teamSelectMenu = Telegraf.Extra
    .markdown()
    .markup((m) => m.inlineKeyboard([{
                text: '🔵',
                callback_data: 'team_select_🔵'
            },
            {
                text: '🔴',
                callback_data: 'team_select_🔴'
            },
        ], {
            columns: 2
        })
        .oneTime()
        .resize());

bot
    .use(Session())
    .use(getUserData())
    .start((ctx) => ctx.reply('Bot started!'))
    .command('game',
        (ctx) => {
            let chatId = ctx.update.message.chat.id;

            Game.begin(chatId)
                .then(async (gameData) => {

                    const CAPTION = (gameData.TEAM_TURN == "🔵") ? "🟦🟦🟦🟦 Первой ходит СИНЯЯ КОМАНДА 🟦🟦🟦🟦" : "🟥🟥🟥🟥 Первой ходит КРАСНАЯ КОМАНДА 🟥🟥🟥🟥";

                    await ctx.replyWithPhoto({
                        source: `games/${chatId}/game.png`
                    }, {
                        caption: CAPTION
                    })
                    // Ожидаем подсказку от капитана для показа меню выбора
                    // ctx.reply('Выберите картинку:', picSelectMenu(chatId))
                })
        })
    .command('key',
        (ctx) => {
            let chatId = ctx.update.message.chat.id,
                userName = ctx.update.message.from.first_name + " " + ctx.update.message.from.last_name,
                userId = ctx.update.message.from.id;

            if (!ctx.session.team) {
                ctx.reply("Чтобы стать капитаном, нужно сначала выбрать команду");
                return;
            }

            const captains = {
                CAPTAIN_BLUE: (ctx.session.game) ? ctx.session.game.CAPTAIN_BLUE : null,
                CAPTAIN_RED: (ctx.session.game) ? ctx.session.game.CAPTAIN_RED : null,
            }

            const
                isAlreadyCaptain = ctx.session.isCaptain || captains.CAPTAIN_RED == userId || captains.CAPTAIN_BLUE == userId,
                isCanBeCaptainBlue = !captains.CAPTAIN_BLUE && ctx.session.team == "🔵",
                isCanBeCaptainRed = !captains.CAPTAIN_RED && ctx.session.team == "🔴";

            if (isAlreadyCaptain || isCanBeCaptainBlue || isCanBeCaptainRed) {
                bot.telegram.sendMediaGroup(userId, [{
                            type: "photo",
                            media: {
                                source: `games/${chatId}/game.png`
                            }
                        },
                        {
                            type: "photo",
                            media: {
                                source: `games/${chatId}/key.png`
                            }
                        },
                    ])
                    .then(() => {
                        if (!isAlreadyCaptain)
                            Game
                            .setCaptain(userId, chatId, ctx.session.team)
                            .then(response => {
                                if (response == false) {
                                    new Error("Can't find session to find captain")
                                    return
                                }
                                ctx.reply(`${(ctx.session.team == "🔴")?"🟥":"🟦"} ${userName} становится капитаном ${(ctx.session.team == "🔴")?"КРАСНОЙ":"СИНЕЙ"} команды!`);
                            })
                    })
                    .catch(e => {
                        ctx.reply("Чтобы получить ключи, нужно хотя бы раз написать боту в ЛС")
                        console.log(e);
                    })
            } else if (!isCanBeCaptainBlue || !isCanBeCaptainRed) {
                // Not captain, but he's team already has captain
                if (!isCanBeCaptainBlue)
                    ctx.reply("🟦 У СИНЕЙ команды уже есть капитан 🟦");
                else
                    ctx.reply("🟥 У КРАСНОЙ команды уже есть капитан 🟥");
            } else {
                ctx.reply("Уже выбрано 2 капитана");
            }
        })
    .command('teams',
        (ctx) => {
            let chatId = ctx.update.message.chat.id;
            Game
                .getTeams(chatId)
                .then(teams => {
                    if (!teams) {
                        ctx.reply("Команды не созданы");
                        return;
                    }

                    let msg = "▫️▫️▫ КОМАНДЫ ▫️▫️▫️",
                        redTeam = "",
                        blueTeam = "";

                    teams.forEach(user => {
                        if (user.COLOR == "🔴")
                            redTeam += `\n🔴 ${user.NAME} (@${user.USERNAME})${(user.isCaptain)?"👑":""}`
                        else
                            blueTeam += `\n🔵 ${user.NAME} (@${user.USERNAME})${(user.isCaptain)?"👑":""}`
                    });

                    ctx.reply(msg + redTeam + blueTeam, {
                        disable_notification: true
                    });
                })
        })
    .command('team',
        (ctx) => {
            ctx.reply('Выберите команду:', teamSelectMenu)
        })
    .action(/^pic_select_(.+)/, (ctx) => {
        const
            chatId = ctx.update.callback_query.message.chat.id,
            pos = ctx.match[1];
        Game.update(chatId, pos).then(res => {
            console.log(res)

            ctx.reply(`Попыток осталось: ${999}`, picSelectMenu(ctx.session.game.FIELD))
        })
        // console.log(ctx.session.game.KEY[ctx.match[1]])
    })
    .action(/^team_select_(.+)/, (ctx) => {
        let chatId = ctx.update.callback_query.message.chat.id,
            team = ctx.match[1],
            userData = ctx.update.callback_query.from,
            userName = userData.first_name + " " + userData.last_name;

        User.setTeam(userData, chatId, team)
        ctx.editMessageText(`${(team == "🔵")?"🔵":"🔴"} ${userName} теперь в ${(team == "🔵")?"СИНЕЙ":"КРАСНОЙ"} КОМАНДЕ`);
    })
    .hears(/^([\W]+) ([0-8])$/, (ctx) => {
        // Accept tip conditions
        const
            isGameCreated = !!ctx.session.game,
            isUserCaptain = ctx.session.isCaptain,
            isTeamTurn = ctx.session.game.TEAM_TURN == ctx.session.team,
            isFirstTip = ctx.session.game.ATTEMPTS == 0;

        if (!isGameCreated || !isUserCaptain || !isTeamTurn || !isFirstTip) return;

        let chatId = ctx.update.message.chat.id,
            user = {
                id: ctx.update.message.from.id,
                name: ctx.update.message.from.first_name + " " + ctx.update.message.from.last_name
            },
            tip = ctx.match[1],
            attemptsAmount = parseInt(ctx.match[2]),
            formatWord = (attemptsAmount == 1) ? "слово" : (attemptsAmount < 5) ? "слова" : "слов";

        ctx.deleteMessage(ctx.update.message.message_id)
            .catch(e => console.log("Bot doesn't have rights to delete TIP message"));

        Game.setAttempts(chatId, attemptsAmount).then(() => {
            ctx.reply(`${(ctx.session.team == "🔵")?"🔵":"🔴"} ${user.name} связал ${attemptsAmount} ${formatWord} ассоциацией \n${tip.toUpperCase()}`)
                .then(() => {
                    ctx.reply(`Попыток осталось: ${attemptsAmount + 1}`, picSelectMenu(ctx.session.game.FIELD))
                });
        })
    })
    .command("report", (ctx) => {
        ctx.reply("Решите голосованием, нарушила ли команда правила")
    })
    .launch()
    .catch(e => {
        console.log(e);
        console.log("============================================");
        console.log("Ну вот, все сломалось");
    });