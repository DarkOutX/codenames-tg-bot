const User = require('./../controllers/User')
const Game = require('./../controllers/Game')

const getUserData = () => (ctx, next) => {

    const 
        chatId = (ctx.update.callback_query)?ctx.update.callback_query.message.chat.id:ctx.update.message.chat.id;
        data = (ctx.update.callback_query) 
                        ? ctx.update.callback_query
                        : ctx.update.message,
        user = {
            id: data.from.id
        };

    let teamColor = User.getTeam(user.id, chatId)
    let game = await Game.getMainData(chatId) || { CAPTAIN_BLUE: 0, CAPTAIN_RED: 0 }

    ctx.session.isCaptain = user.id == game.CAPTAIN_BLUE || user.id == game.CAPTAIN_RED;
    ctx.session.team = teamColor;
    ctx.session.game = game;

    return next();
}

module.exports = getUserData
